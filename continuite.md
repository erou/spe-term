## Continuité

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-term/pdfs/cours-continuite.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-term/pdfs/exos-continuite.pdf)

---

### <i class="fa fa-code" aria-hidden="true"></i> Algorithmes

Si une fonction continue $$f$$ s'annule entre $$a$$ et $$b$$, on peut
chercher un point d'annulation sur $$[a; b]$$ en coupant en deux l'intervalle,
puis en regardant sur quel sous-intervalle le signe de $$f$$ change pour ensuite
continuer la même procédure jusqu'à obtenir un sous-intervalle assez petit.

<iframe src="https://trinket.io/embed/python/57555eeadb7b" width="100%" height="450" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe>

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

Yvan Monka, dans la continuité de son travail, propose de nouveau une série de
vidéos sur ce chapitre.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=9EF-JFr4AmwqSVE1&amp;list=PLVUDmbpupCaru94MHSdOLsjrM0Ppe2kfv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

On a vu que les fonctions dérivables étaient forcément continues. La réciproque
est fausse, même si toutes les fonctions rencontrées dans ce chapitre étaient à
la fois continues et dérivables. Il existe même des fonctions continues en tout
point mais dérivables en aucun point : la figure ci-dessous montre la
construction d'une telle fonction. Elle est due aux mathématiciens Karl
Weierstrass et Leopold Kronecker. Voir [l'article
Wikipédia](https://fr.wikipedia.org/wiki/Fonction_de_Weierstrass) pour plus de
renseignements.

<div style="text-align:center">
<img
src="https://upload.wikimedia.org/wikipedia/commons/2/29/Weierstrass_Animation.gif" width="70%" alt="La fonction de Weiestrass.">
</div>
