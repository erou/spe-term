## Compléments sur la dérivation

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-term/pdfs/cours-complements-derivation.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-term/pdfs/exos-complements-derivation.pdf)

---

### <i class="fa fa-code" aria-hidden="true"></i> Algorithmes

Pas d'algorithme dans ce chapitre.

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

Cette fois-ci double dose de monsieur Monka, avec une série sur la dérivation et
une vidéo sur la convexité !

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=IakAC9qNc8bvq4d4&amp;list=PLVUDmbpupCaolmlZsgQvpuNlXlUQ5D5vS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/gge4xdn6cFA?si=PnWuOtW0myJ8v8mg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

On pourra essayer de dériver une fonction trois fois, puis quatre fois, ... ,
puis $$n$$ fois ! De manière générale, on note $$f^{(n)}$$ la dérivée $$n$$-ième
de la fonction $$f$$. Certaines fonctions peuvent être dérivées une infinité de
fois, on dit alors qu'elles sont « lisses » ou « régulières ». On peut jouer à
classer les fonctions en terme de régularité, voir [l'article
Wikipédia](https://fr.wikipedia.org/wiki/Classe_de_r%C3%A9gularit%C3%A9) à ce
sujet. La morale de l'histoire : plus une fonction est régulière, plus on pourra
en dire des choses poussées.

<div style="text-align:center">
<img alt="Meme qui expliquent l'avancée en compétence liée à la dérivation." src="https://i.imgflip.com/9b4fni.jpg" width="80%">
</div>
