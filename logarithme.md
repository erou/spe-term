## Fonction logarithme népérien

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-term/pdfs/cours-logarithme.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-term/pdfs/exos-logarithme.pdf)

---

### <i class="fa fa-code" aria-hidden="true"></i> Algorithmes

L'algorithme de Briggs permet de calculer le logarithme (ici, en base $$10$$)
d'un nombre entre $$1$$ et $$10$$. Cet algorithme permit à [Henry
Briggs](https://fr.wikipedia.org/wiki/Henry_Briggs) de publier les premières
tables de logarithmes décimaux, intitulées *Arithmetica logarithmica*, en 1624.

L'idée, ingénieuse, est une variante d'un algorithme par dichotomie. À la place
de considérer la moyenne arithmétique de deux nombres $$a$$ et $$b$$

<div style="text-align:center">
$${\displaystyle \frac{a+b}{2}}$$
</div>

comme dans le cas classique, on considère la moyenne géométrique

<div style="text-align:center">
$${\displaystyle \sqrt{ab}}$$
</div>

et on utilise l'identité

<div style="text-align:center">
$${\displaystyle \ln(\sqrt{ab})=\frac{\ln(a)+\ln(b)}{2}}$$
</div>

pour remarquer que les logarithmes, eux, vont converger vers la valeur
recherchée par dichotomie, de la manière habituelle.

<iframe src="https://trinket.io/embed/python/6949c966a2f1" width="100%" height="400" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe>


---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

Une vidéo pour comprendre le cours sur le logarithme népérien.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/VJns0RfVWGg?si=-eZzdxJa570peavN" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

Une vidéo expliquant un exemple d'utilisation des logarithmes dans la vie
courante, pour calculer le pH des piscines. Évidemment il y a beaucoup d'autres
applications !

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/zzu2POfYv0Y?si=h-rf7dFD1_gnPbak" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
