## Orthogonalité et distances dans l'espace

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-term/pdfs/cours-orthogonalite.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-term/pdfs/exos-orthogonalite.pdf)

---

### <i class="fa fa-code" aria-hidden="true"></i> Algorithmes

Pas d'algorithme dans ce chapitre.

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

La suite de la géométrie selon M. Monka.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/pMQBaCqLPsQ?si=VTxShgRExNFB0piS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

Pour continuer de faire de la géométrie en dimension supérieure, on peut définir
un produit scalaire plus sophistiqué et travailler dans ce qu'on appelle un
espace préhilbertien, ou mieux, un [espace de
Hilbert](https://fr.wikipedia.org/wiki/Espace_de_Hilbert). Les techniques vues
en dimension trois se généralisent et on peut résoudre des problèmes
d'orthogonalité, trouver des angles, mesure des longueurs, *etc.* !
