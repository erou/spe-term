## <i class="fa fa-comments-o" aria-hidden="true"></i> Le grand oral

Cette page a pour vocation de rassembler des ressources pour vous aider à
préparer l'épreuve du grand oral, en particulier avec la spécialité
mathématiques.

### <i class="fa fa-info-circle" aria-hidden="true"></i> Description de l'épreuve

Une description complète, à destination des élèves, est disponible sur
[« Réussir au lycée »][reussir-lycee]. Une présentation encore plus complète est
disponible sur [Eduscol][eduscol].

**<i class="fa fa-hourglass-half" aria-hidden="true"></i> Durée :**

L'épreuve dure 40 minutes.

- 20 minutes de préparation ;
- 20 minute d'oral, dont
    - 10 minutes de présentation de sa problématique ;
    - 10 minutes d'échange avec le jury.

**<i class="fa fa-bullseye" aria-hidden="true"></i> Objectifs :**

- Présenter une problématique pertinente.
- Être convaincant⋅e.
- Utiliser ses connaissances judicieusement.

Les détails de la grille d'évaluation sont en annexe de [ce
document][grille-eval], avec une explication détaillée de cette grille dans [ce
document][charte-exam].

### <i class="fa fa-compass" aria-hidden="true"></i> Ressources pour se préparer

- Une série de vidéos pour apprendre à bien respirer, maîtriser sa voix et gérer
  son stress est disponible dans le manuel [Sesamath](sesamath), page 441.
- Des conseils pratiques en tout genre sont trouvables dans le manuel [Le livre
  scolaire][livre-scolaire], et [une page en ligne][oral-livre-scolaire] recense
  tout cela. En particulier, cela parle de
    - comment gérer un oral ;
    - comment bien discuter avec le jury ;
    - savoir trouver des sujets, les cadrer, en tirer des problématiques 
      intéressantes ;
    - savoir faire des recherches ;
    - savoir structurer une présentation ;
    - comment s'entraîner.

### <i class="fa fa-shopping-cart" aria-hidden="true"></i> Choisir un sujet

Un [groupe de travail][classification-sujets] propose de classer les sujets
possibles en 4 catégories.

1. Un problème de modélisation : on construit un modèle mathématique pour
   répondre à un questionnement issu d’un autre domaine, vie réelle ou autre
   spécialité. Dans ce dernier cas, le sujet peut même être bi-disciplinaire.
2. Un problème mathématique qui a évolué au cours de l’histoire, ou une notion
   rattachée à un intérêt porté à un mathématicien ou une mathématicienne.
3. Un problème débouchant sur un champ vaste des mathématiques, avec notamment
   des changements de registres possibles.
4. Un problème plus modeste, proche du programme, mais pour lequel l’élève a un
   réel intérêt : ce peut être un point qu’on apprécie sur lequel on a
   pris du recul, mais aussi un point qui nous a posé des difficultés et qu’on
   a progressivement cerné.

Si votre sujet ne tombe dans aucune de ces catégories, il n'y a *peut-être* pas
assez de mathématiques dedans. Le plus simple est d'en discuter ensemble pour
vérifier.
 
[eduscol]: https://eduscol.education.fr/729/presentation-du-grand-oral
[reussir-lycee]: https://www.education.gouv.fr/reussir-au-lycee/baccalaureat-comment-se-passe-le-grand-oral-100028
[grille-eval]: https://eduscol.education.fr/document/52926/download
[livre-scolaire]: https://www.lelivrescolaire.fr/manuels/mathematiques-specialite-terminale-2020
[oral-livre-scolaire]: https://www.lelivrescolaire.fr/page/13249530
[sesamath]: https://manuel.sesamath.net/numerique/?ouvrage=mstsspe_2020
[rapport-ig]: https://eduscol.education.fr/media/3896/download
[rapport-ig-maths]: https://eduscol.education.fr/media/3916/download
[rapport-ig-2023]: https://eduscol.education.fr/document/46243/download?attachment
[classification-sujets]: https://pedagogie.ac-strasbourg.fr/fileadmin/pedagogie/mathematiques/Lycee/Oral_au_lycee__grand_oral/Vers_une_question_en_maths_pour_le_grand_oral_V2.pdf
[charte-exam]: https://mathematiques.ac-normandie.fr/IMG/pdf/charte_acade_mique_go_vf-1.pdf
