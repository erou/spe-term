## <i class="fa fa-compass" aria-hidden="true"></i> Ressources supplémentaires

Nous disposons d'un temps limité en classe, et vous trouverez peut-être parfois
que nous avons été vite sur une notion ou que nous n'avons pas fait assez
d'exercices.

Cette page vous donne quelques ressources, au cas où vous voudriez retravailler
le cours avec un support différent de celui vu en classe, ou si vous désirez
faire des exercices supplémentaires. N'ayez pas peur de faire des exercices non
corrigés : si vous ne trouvez pas la solution, je répondrai à vos questions avec
plaisir.

### <i class="fa fa-book" aria-hidden="true"></i> Des manuels

Ces manuels sont accessibles gratuitement en ligne.

+ [Le livre
  scolaire](https://www.lelivrescolaire.fr/manuels/mathematiques-specialite-terminale-2020)
+ [Sésamath](https://manuel.sesamath.net/numerique/?ouvrage=mstsspe_2020)
+ [Maths cours](https://www.maths-cours.fr/) : pas vraiment un manuel, mais un
  cours de Terminale.

### <i class="fa fa-youtube-play" aria-hidden="true"></i> Chaînes YouTube

+ [M@ths et tiques](https://www.youtube.com/@YMONKA), la chaîne d'Yvan Monka,
  regroupe des milliers de vidéos sur toutes les mathématiques du collège et du
  lycée, c'est une mine d'or.
+ [EXERCICES MATHS](https://www.youtube.com/@videomaths), qui comme son nom
  l'indique propose de résoudre des exercices.
+ [J'ai 20 en maths](https://www.youtube.com/@Jai20enmaths), du cours et des
  exercices corrigés.
+ [Hans Amble - Maths au Lycée](https://www.youtube.com/@maths-lycee), également
  des cours et des résolutions d'exercices.
+ [KIFFELESMATHS](https://www.youtube.com/@KIFFELESMATHS), une autre chaîne proposant des
  cours et des exercices niveau lycée.

### <i class="fa fa-graduation-cap" aria-hidden="true"></i> Des sujets du bac

Les sujets des sessions précédentes du baccalauréat sont facilement trouvables
sur Internet grâce à un moteur de recherche. Les corrections ne sont pas non
plus très difficiles à trouver.

+ [Session 2024](https://www.education.gouv.fr/reussir-au-lycee/bac-2024-les-sujets-des-epreuves-ecrites-de-specialite-414414)
+ [Session 2023](https://www.education.gouv.fr/reussir-au-lycee/bac-2023-les-sujets-des-epreuves-ecrites-de-specialite-357743)

### <i class="fa fa-code" aria-hidden="true"></i> Travailler Python

Les sujets de bac comportent une question sur Python, voici ce que comporte le
programme.

+ Notion de variable :
    + type de variable ;
    + affectation de variable.
+ Calculs basiques :
    + addition, soustraction ;
    + multiplication, division ;
    + exponentiation (puissances).
+ Instruction conditionnelle :
    + boucle `for` ;
    + boucle `while`.
+ Notion de fonction.

Si vous n'êtes pas à l'aise avec ce langage, vous pouvez consulter les
ressources suivantes.

+ [Le cahier d'algorithmique et de
  programmation](https://www.lelivrescolaire.fr/page/13044469) du livre
scolaire, il contient des fiches de cours et propose des exercices interactifs.
+ [Ce
  tutoriel](https://openclassrooms.com/fr/courses/7168871-apprenez-les-bases-du-langage-python)
du site Openclassrooms, pas forcément destiné aux élèves de Terminale, mais qui
revient sur les bases de Python.
+ D'autres sites comme [Python doctor](https://python.doctor) permettent de travailler Python de manière bien plus avancée que ce que le programme prévoit. 
