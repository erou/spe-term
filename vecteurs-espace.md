## Vecteurs, droites et plans de l'espace

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-term/pdfs/cours-vecteurs-espace.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-term/pdfs/exos-vecteurs-espace.pdf)

---

### <i class="fa fa-code" aria-hidden="true"></i> Algorithmes

Pas d'algorithme dans ce chapitre.

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

M. Monka, à vous de jouer !

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/EoT48VtnUJ4?si=T2ufMvzTbgiwODrz" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

On pourrait essayer de travailler avec des vecteurs en quatre dimension au lieu
de trois ! Mais pourquoi s'arrêter à quatre ? En fait on pourrait s'intéresser à
des « espaces » de dimension arbitraire, voire même de dimension *infinie* !
Ces espaces sont appelés des [espaces
vectoriels](https://fr.wikipedia.org/wiki/Espace_vectoriel) et sont étudiés dans
le supérieur.
