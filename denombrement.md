## Dénombrement

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-term/pdfs/cours-denombrement.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-term/pdfs/exos-denombrement.pdf)

---

### <i class="fa fa-code" aria-hidden="true"></i> Algorithmes

À venir :

+ génération de la liste des coefficients binomiaux à l'aide de la relation de Pascal
+ génération des permutations d'un ensemble fini, ou tirage aléatoire d'une
  permutation
+ génération des parties à 2; 3 élements d'un ensemble fini

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

M. Monka nous apprend à compter les choses difficiles à compter.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=eMT4N0S0cM3ctDQO&amp;list=PLVUDmbpupCaru1ReP966kn39zbpkCKm-A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

Le dénombrement vu dans ce chapitre se généralise en toute une branche des
mathématiques, qu'on appelle la
[combinatoire](https://fr.wikipedia.org/wiki/Combinatoire). Il y a de nombreux
domaines de spécialisation, la combinatoire algébrique, géométrique, analytique,
topologique...
