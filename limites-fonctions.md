## Limites de fonctions

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-term/pdfs/cours-limites-fonctions.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-term/pdfs/exos-limites-fonctions.pdf)

---

### <i class="fa fa-code" aria-hidden="true"></i> Algorithmes

Pas d'algorithme au programme dans ce chapitre.

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

Une *playlist* d'Yvan Monka, toujours très complète, permet de revenir sur tout le
chapitre.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=vSx9Getwna9YjYJd&amp;list=PLVUDmbpupCarS4Qp45vTwsEGYMOJtgBxE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

Comme on l'a vu dans le chapitre, il y a plusieurs types de limites en
$$\pm\infty$$, et on pourrait aller encore plus loin dans la classification de
ces limites. On appelle ce travail l'étude des « branches infinies », et vous
avez un [résumé
ici](https://www.bibmath.net/dico/index.php?action=affiche&quoi=./b/branchinf.html).
