## Succession d'épreuves indépendantes et loi binomiale

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-term/pdfs/cours-loi-binomiale.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-term/pdfs/exos-loi-binomiale.pdf)

---

### <i class="fa fa-code" aria-hidden="true"></i> Algorithmes

À venir.

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

À venir.

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

À venir.
